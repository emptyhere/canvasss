const isSameAxis = (shape, axis) => {
  return ((shape.top_left[axis] === shape.top_right[axis]) && 
  (shape.top_left[axis] === shape.bot_right[axis]) &&
  (shape.top_left[axis] === shape.bot_left[axis]) &&
  (shape.top_right[axis] === shape.bot_right[axis]) &&
  (shape.top_right[axis] === shape.bot_left[axis]) &&
  (shape.bot_right[axis] === shape.bot_left[axis]))
}

const getExtendedSide = (shape, axis) => {
  return (((shape.top_left[axis] === shape.top_right[axis]) && 
  (shape.top_left[axis] === shape.bot_right[axis])) ||
  ((shape.top_right[axis] === shape.top_left[axis]) && 
  (shape.top_right[axis] === shape.bot_right[axis])) ||
  ((shape.bot_right[axis] === shape.top_left[axis]) && 
  (shape.bot_right[axis] === shape.bot_left[axis])) ||
  ((shape.bot_left[axis] === shape.top_left[axis]) && 
  (shape.bot_left[axis] === shape.top_right[axis])) ||
  ((shape.top_left[axis] === shape.bot_left[axis]) && 
  (shape.top_left[axis] === shape.bot_right[axis])) ||
  ((shape.top_right[axis] === shape.bot_left[axis]) && 
  (shape.top_right[axis] === shape.bot_right[axis])) ||
  ((shape.top_left[axis] === shape.top_right[axis]) && 
  (shape.top_right[axis] === shape.bot_left[axis])))
}

export { isSameAxis, getExtendedSide }